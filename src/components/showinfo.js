import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import LandingPage from './landingpage';
import AboutMe from './aboutme';
import Contact from './contact';
import Projects from './projects';
import Resume from './resume';
import Home from './Home';
import Zahirgaa from './zahirgaa';
import Surgalt from './surgalt';
import Erdem from './erdem';
import { Login } from './login';
import App from '../App';


const ShowInfo = () => (
    <BrowserRouter>
    <App />
      </BrowserRouter>
)

export default ShowInfo;
