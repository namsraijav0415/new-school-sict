import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import Education from './education';
import Experience from './experience';
import Skills from './skills';
import StickyFooter from 'react-sticky-footer';

const school = require('../assets/homeImg.jpg');
const shutis = require('../assets/shutis.png');

const news1 = require('../assets/news1.jpeg');
const news2 = require('../assets/news2.jpeg');
const news3 = require('../assets/news3.jpeg');
const news4 = require('../assets/news4.jpeg');
const news5 = require('../assets/news5.jpeg');
const news6 = require('../assets/news6.jpeg');
const news7 = require('../assets/news7.jpeg');
const news8 = require('../assets/news8.jpeg');

class Home extends Component {
  render() {
    return (
      <div>
        {/* <h1>hello</h1> */}
        <Grid>
          <Cell col={8}>
            <div style={{ textAlign: 'center' }}>
              <img
                // src="https://www.shareicon.net/download/2015/09/18/103157_man_512x512.png"
                src={school}
                alt="avatar"
                style={{ height: '400px' }}
              />
            </div>
            <div className="medee">
              <text style={{ color: 'white' }}>
                Мэдээ, мэдээлэл
              </text>
            </div>
            <div className="zarmedee">
              <ZarMedee data={data2} />
            </div>
            {/* 
            <h2 style={{ paddingTop: '2em' }}>Paul Hanna</h2>
            <h4 style={{ color: 'grey' }}>Programmer</h4>
            <hr style={{ borderTop: '3px solid #833fb2', width: '50%' }} />
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            <hr style={{ borderTop: '3px solid #833fb2', width: '50%' }} />
            <h5>Address</h5>
            <p>1 Hacker Way Menlo Park, 94025</p>
            <h5>Phone</h5>
            <p>(123) 456-7890</p>
            <h5>Email</h5>
            <p>someone@example.com</p>
            <h5>Web</h5>
            <p>mywebsite.com</p>
            <hr style={{ borderTop: '3px solid #833fb2', width: '50%' }} /> */}
          </Cell>
          <Cell className="resume-right-col" col={4}>
            <h3>Зар мэдээ </h3>
            <ListOfNews data={data} />


          </Cell>
        </Grid>
        {/* <Footer /> */}
        <StickyFooter
          bottomThreshold={50}
          normalStyles={{
            backgroundColor: "#02459e",
            padding: "2rem",
            height: '400',
          }}
          stickyStyles={{
            backgroundColor: '#833ab4',
            padding: "2rem",
            height: 400,
            flexDirection: 'row',
          }}
        >
          <div className="footerGod">
            <div className="footerfirstdiv">
              <img
                // src="https://www.shareicon.net/download/2015/09/18/103157_man_512x512.png"
                src={shutis}
                alt="avatar"
                style={{ height: '100px', width: '50px' }}
              />
              <text style={{ marginTop: 10, color: 'white', fontSize: 18 }} >Монгол Улсын Шинжлэх Ухаан Технологийн Их Сургууль</text>

            </div>
            <div className="secondfooterfirstdiv">
              <text style={{ fontSize: 20, color: 'yellow' }}>
                Үндсэн цэс
          </text>
              <text style={{ marginTop: 10, color: 'white', fontSize: 13 }} >Нүүр  /  Бидний тухай  /  Захиргаа  /  Сургалт  /  Эрдэм шинжилгээ  /  Салбарууд  /  Оюутан</text>

            </div>
            <div className="secondfooterfirstdiv">
              <text style={{ fontSize: 20, color: 'yellow' }}>
                Бидэнтэй холбогдох
          </text>
              <text style={{ marginTop: 10, color: 'white', fontSize: 13 }} >Монгол улс, Улаанбаатар хот
              Баянзүрх дүүрэг,22-р хороо
              Утас: 976-70151333
Сургалтын алба: 70152333</text>
            </div>
          </div>
        </StickyFooter>
      </div>
    )
  }
}

export default Home;

function ListOfNews({ data }) {
  return <div>
    {data.map((item, index) => {
      return <div style={{
        padding: 10,
        borderBottomWidth: 10,
        marginTop: 10,
        borderColor: 'black',
        borderBottom: '2px solid gray'
      }}>
        <a style={{ color: '#000000' }} href="#">{item.text}</a>
      </div>
    })}
  </div>
}

function ZarMedee({ data }) {
  return <div>
    {data.map((item, index) => {
      return <div style={{
        padding: 10,
        borderBottomWidth: 10,
        marginTop: 10,
        borderColor: 'black',
        borderBottom: '2px solid gray',
        flexDirection: 'column',
      }}>
        <div className="imgTxtcontainer">
          <img src={item.image} style={{ width: 180, height: 130, marginLeft: 8, }} />
          <div className="zarTitle">
            <a style={{ color: '#000000', fontSize: 17, width: 300, color: 'blue' }} href="#">{item.text}</a><br/>
            <text style={{fontSize: 13}}>{item.text}</text>
          </div>
        </div>
      </div>
    })}
  </div>
}

var style = {
  backgroundColor: "#F8F8F8",
  borderTop: "1px solid #E7E7E7",
  textAlign: "center",
  padding: "20px",
  position: "fixed",
  left: "0",
  bottom: "0",
  height: "60px",
  width: "100%",
}

var phantom = {
  display: 'block',
  padding: '20px',
  height: '60px',
  width: '100%',
}

function Footer({ children }) {
  return (
    <div>
      <div style={phantom} />
      <div style={style}>
        {children}
      </div>
    </div>
  )
}

const data = [{ id: 1, text: 'МХТС-ийн Магистр, Бакалавр оюутны "ШИЛДЭГ ӨГҮҮЛЭЛ" шалгаруулах удирдамж', date: '20.04.02 16:49:27' },
{ id: 2, text: 'МЭДЭГДЭЛ: ШУТИС-ийн нийт профессор багш, ажилтнууд, оюутан суралцагчдын анхааралд', date: '20.04.02 16:49:27' },
{ id: 3, text: '"INFONET&SEC-2020" ЭРДЭМ ШИНЖИЛГЭЭНИЙ ХУРЛЫН УДИРДАМЖ', date: '20.04.02 16:49:27' },
{ id: 4, text: 'МХТС-ийн нэрэмжит “Инженерчлэлийн эдийн засаг”-ийн НЭЭЛТТЭЙ олимпиадын зарлал', date: '20.04.02 16:49:27' },
{ id: 5, text: 'МХТС-ийн нэрэмжит “Инженерчлэлийн эдийн засаг”-ийн НЭЭЛТТЭЙ олимпиадын удирдамж', date: '20.04.02 16:49:27' },
{ id: 6, text: '"Мэдээлэл, холбооны салбарын хөгжилд бидний гүйцэтгэх үүрэг-2019" сэдэвт магистр, доктор оюутны эрдэм шинжилгээний хурлын удирдамж', date: '20.04.02 16:49:27' },
{ id: 7, text: 'Япон улсад ажиллах хүсэлтэй залууст зориулсан танилцуулга уулзалт болно', date: '20.04.02 16:49:27' },
{ id: 8, text: 'ОХУ-ын Эрхүүгийн Үндэсний Судалгааны Техникийн Их Сургууль, ШУТИС-ийн МХТС-тай хамтарсан магистрын 1+1 хөтөлбөрт урьж байна.', date: '20.04.02 16:49:27' },]

const data2 = [
  {
    id: 1,
    title: 'ДОКТОРЫН ЗЭРЭГ ХАМГААЛЛАА',
    text: '“1000 инженер” Инженер Технологийн Дээд Боловсролын төсөл нь Монгол Улсад өндөр мэдлэг, ур чадвар бүхий инженер, технологийн',
    date: '20.04.12 16:33:46',
    image: news1,
  },
  {
    id: 2,
    title: 'ШУТИС-ийн багш, ажилчдын “Спортын наадам-2020” тэмцээнд амжилттай оролцлоо',
    text: 'МЭДЭГДЭЛ: ШУТИС-ийн нийт профессор багш, ажилтнууд, оюутан суралцагчдын анхааралд',
    date: '20.04.02 16:49:27',
    image: news2,
  },
  {
    id: 3,
    title: '"INFONET&SEC-2020" ЭРДЭМ ШИНЖИЛГЭЭНИЙ ХУРЛЫН УДИРДАМЖ',
    text: 'Мэдээллийн сүлжээгээр хил хязгааргүй орон зайд, цаг хугацааг үл харгалзан асар их мэдээлэл урсах болсон өнөө үед үндэсний аюулгүй',
    date: ' 20.01.20 12:59:57',
    image: news3,
  },
  {
    id: 4,
    title: 'ЗӨВЛӨХ, МЭРГЭШСЭН ИНЖЕНЕРҮҮДЭД ЗЭРЭГ ОЛГОВ',
    text: 'Харилцаа холбоо, мэдээллийн технологийн газрын дэргэдэх мэдээлэл, холбооны технологийн салбарын мэргэжлийн магадлан итгэмжлэх',
    date: '19.12.22 17:18:51',
    image: news4,
  },
  {
    id: 5,
    title: 'ШУТИС-ийн МХТС-ийн нэрэмжит “Инженерчлэлийн эдийн засаг”-ийн НЭЭЛТТЭЙ, анхдугаар олимпиад амжилттай болж өндөрлөлөө',
    text: 'МХТС-ийн Холбооны салбарын санаачилгаар зохион байгуулагдсан “Инженерчлэлийн эдийн засаг”-ийн НЭЭЛТТЭЙ, анхдугаар олимпиадын',
    date: ' 19.11.28 17:03:01',
    image: news5,
  },
  {
    id: 6,
    title: 'Доктор (Ph.D), профессор Г.Цогбадрахын нэрэмжит “Мэдээлэл, Холбооны салбарын хөгжилд бидний гүйцэтгэх үүрэг-2019” сэдэвт намрын улирлын магистр, доктор оюутны эрдэм шинжилгээний хурал боллоо',
    text: 'Доктор (Ph.D), профессор Г.Цогбадрахын нэрэмжит “Мэдээлэл, Холбооны салбарын хөгжилд бидний гүйцэтгэх үүрэг-2019” сэдэвт',
    date: '19.11.27 15:42:09',
    image: news6,
  },
  {
    id: 7,
    title: 'Ирээдүйд Япон улсад ажиллах хүсэлтэй оюутнуудад зориулсан танилцуулгыг Японы “DAIKI Engineering” ХК-аас хийнэ.',
    text: '“DAIKI Engineering” ХК нь автомашин, IT, усан онгоц, нисэх онгоц, галт тэрэг, цахилгаан тоног төхөөрөмж зэрэг төрөл бүрийн үйлдвэрлэлийн',
    date: '19.10.28 13:42:51',
    image: news7,
  },
  {
    id: 8,
    title: 'ШУТИС-ийн Шилдэг ном сурах бичиг, орчуулгын бүтээл, нэг сэдэвт бүтээл, гарын авлага шалгаруулах уралдаан',
    text: 'ШУТИС-ийн 60 жилийн ойд зориулсан ШУТИС-ийн Шилдэг ном сурах бичиг, гарын авлага, орчуулгын бүтээл, нэг сэдэвт бүтээл шалгаруулах',
    date: ' 19.10.16 10:41:16',
    image: news8,
  },]