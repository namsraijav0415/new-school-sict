import React, { Component } from "react";
import { Link, Redirect } from 'react-router-dom';

const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const formValid = ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false);
  });

  return valid;
};

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: null,
      lastName: null,
      email: '',
      password: null,
      loggedin: false,
      formErrors: {
        firstName: "",
        lastName: "",
        email: "",
        password: ""
      }
    };
  }

  handleSubmit = e => {
    e.preventDefault();

    if (formValid(this.state)) {
      console.log(`
        --SUBMITTING--
        First Name: ${this.state.firstName}
        Last Name: ${this.state.lastName}
        Email: ${this.state.email}
        Password: ${this.state.password}
      `);
    } else {
      console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
    }
  };

  handleChange = (event) => {
    this.setState({email: event.target.value});
  }

  changePass = (event) => {
    this.setState({password: event.target.value});
  }

  onclick = () => {
      const {email, password} = this.state;
      let val = Users.map((item, index) => {return item.name}).indexOf(email)
      console.log('suirliin hejee', Users[val].name == email, email, Users[val]. name)
      if(Users[val].name == email && Users[val].pass == password) {
        this.setState({loggedin: true});
        // <Link to={this.props.myroute} onClick='hello()'>Here</Link>
      }
      else {
        alert('false')
      }
       }

  render() {
    const { formErrors } = this.state;
    if (this.state.loggedin === true) {
      return <Redirect to='/loggedin' />
    }
    return (
      <div className="wrapper">
        <div className="form-wrapper">
          <h1>Нэвтрэх</h1>
          <form onSubmit={this.handleSubmit} noValidate>
            <div className="email">
              <label htmlFor="email">Email</label>
              <input
                className={formErrors.email.length > 0 ? "error" : null}
                placeholder="Email"
                type="email"
                value={this.state.email}
                name="email"
                noValidate
                onChange={this.handleChange}
              />
              {formErrors.email.length > 0 && (
                <span className="errorMessage">{formErrors.email}</span>
              )}
            </div>
            <div className="password">
              <label htmlFor="password">Password</label>
              <input
                className={formErrors.password.length > 0 ? "error" : null}
                placeholder="Password"
                type="password"
                value={this.state.password}
                name="password"
                noValidate
                onChange={this.changePass}
              />
              {formErrors.password.length > 0 && (
                <span className="errorMessage">{formErrors.password}</span>
              )}
            </div>
            <div className="createAccount">
              <button onClick={this.onclick} type="submit">Нэвтрэх</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export {Login};

const Users = [
  {id: 0, roleId:1, name: 'Batorgil@gmail.com', pass: 'Orgio1234'},
  {id: 1, roleId:2, name: 'Munkbat_teacher@gmail.com', pass: '123456'},
  {id: 0, roleId:2, name: 'Bat_enk@sice.edu.mn', pass: '123456'},
  {id: 0, roleId:2, name: 'Namjildorj@gmail.com', pass: '123456'},
]