import React, { Component } from 'react';
import StickyFooter from 'react-sticky-footer';
const shutis = require('../assets/shutis.png');


class Surgalt extends Component {
    render() {
        return (
            <div class="full-height">
                <div className="secondaoubdiv">
                    <div className="ulmedehabout">

                        <div className="headeabout" >
                            <a style={{ marginLeft: 20, color: 'blue', fontSize: 12, }} >Нүүр </a>
                            <text style={{ marginLeft: 5, fontSize: 12, color: 'gray' }}>   / Сургалт </text>
                        </div>
                        <div className="headeaboutText" >
                            <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                                Монгол улсын Боловсролын тухай, дээд боловсролын тухай хуулиуд, тэдгээрт нийцүүлэн Боловсролын асуудал эрхэлсэн төрийн захиргааны төв байгууллагаас гаргасан дүрэм, журмыг баримтлан Монгол Улсын Шинжлэх Ухаан Технологийн Их Сургууль дээд боловсролын зэрэг олгох сургалт зохион байгуулан явуулж байна.
                              </text><br />
                            <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                                Дээд боловсролын дипломын болон бакалаврын зэрэг олгох сургалтын гол зорилго нь бүрэн дунд боловсролтой хүнийг элсүүлэн их сургуулийн мэргэжлийн суурь боловсрол олгож, цаашид бие даан суралцах мэдлэг, чадвар эзэмшсэн мэргэжилтэй боловсон хүчин бэлтгэхэд оршино.
                             </text><br />
                            <text style={{ marginLeft: 17, fontWeight: 'bolder', fontSize: 15, textAlign: 'justify', lineHeight: 2 }}>
                                Цахилгаан холбоо
                            </text>
                            <text style={{ marginLeft: 17, fontWeight: 'bolder', fontSize: 15, textAlign: 'justify', lineHeight: 2 }}>
                                Утасгүй холбоо
                            </text>
                            <text style={{ marginLeft: 17, fontWeight: 'bolder', fontSize: 15, textAlign: 'justify', lineHeight: 2 }}>
                                Сүлжээний технологи
                            </text>
                            <text style={{ marginLeft: 17, fontWeight: 'bolder', fontSize: 15, textAlign: 'justify', lineHeight: 2 }}>
                                Мэдээллийн технологи
                            </text>
                            <text style={{ marginLeft: 17, fontWeight: 'bolder', fontSize: 15, textAlign: 'justify', lineHeight: 2 }}>
                                Мэдээллийн систем
                            </text>
                            <text style={{ marginLeft: 17, fontWeight: 'bolder', fontSize: 15, textAlign: 'justify', lineHeight: 2 }}>
                                Мэдээллийн систем
                            </text>
                            <text style={{ marginLeft: 17, fontSize: 15, fontWeight: 'bolder', textAlign: 'justify', lineHeight: 2 }}>
                                Электрон системийн програм хангамжийн инженер
                            </text>
                            <text style={{ marginLeft: 17, fontSize: 15, fontWeight: 'bolder', textAlign: 'justify', lineHeight: 2 }}>
                                Автоматжуулсан системийн инженер
                            </text>

                            <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                                Электрон системийн автоматжуулалт мэргэжлүүдээр бакалаврын зэрэг олгох сургалтуудыг явуулж байна.
                              </text><br />
                            <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                                Магистрантурын сургалтын гол зорилго нь бакалаврын зэрэгтэй хүнийг элсүүлэн судалгааны арга зүй эзэмшүүлж, шинжлэх ухааны тодорхой чиглэлээр гүнзгийрүүлэн, нарийн мэргэшсэн мэргэжилтэн бэлтгэхэд оршино.
                            </text><br />
                            <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                                Докторантурын сургалтын гол зорилго нь магистрантурт суралцан амжилттай төгссөн буюу их, дээд сургууль төгсөөд мэргэжлийн дагуу ажиллан сургалт, эрдэм шинжилгээний чиглэлээр тодорхой бүтээл туурвиж байгаа, онолын өндөр мэдлэг чадвартай хүмүүсийг элсүүлэн, ахисан түвшний сургалтанд хамруулж, тодорхой сэдвээр судалгааны ажил хийлгэн шинжлэх ухаан, технологийн салбарт бодитой хувь нэмэр оруулах судлаачдыг бэлтгэхэд оршино. Жишээлбэл:

</text><br />
                            <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                                Дээд боловсролын дипломын болон бакалаврын зэрэг олгох сургалтын гол зорилго нь бүрэн дунд боловсролтой хүнийг элсүүлэн их сургуулийн мэргэжлийн суурь боловсрол олгож, цаашид бие даан суралцах мэдлэг, чадвар эзэмшсэн мэргэжилтэй боловсон хүчин бэлтгэхэд оршино.
                             </text><br />
                        </div>
                    </div>
                    <div className="garchig" >
                        <MenuData data={data} />
                    </div>
                </div>
                {/* <div className="footercontainer">  */}
                <StickyFooter
                    bottomThreshold={50}
                    normalStyles={{
                        backgroundColor: "#02459e",
                        padding: "2rem",
                        height: '400',
                    }}
                    stickyStyles={{
                        backgroundColor: '#833ab4',
                        padding: "2rem",
                        height: 400,
                        flexDirection: 'row',
                    }}
                >
                    <div className="footerGod">
                        <div className="footerfirstdiv">
                            <img
                                // src="https://www.shareicon.net/download/2015/09/18/103157_man_512x512.png"
                                src={shutis}
                                alt="avatar"
                                style={{ height: '100px', width: '50px' }}
                            />
                            <text style={{ marginTop: 10, color: 'white', fontSize: 18 }} >Монгол Улсын Шинжлэх Ухаан Технологийн Их Сургууль</text>

                        </div>
                        <div className="secondfooterfirstdiv">
                            <text style={{ fontSize: 20, color: 'yellow' }}>
                                Үндсэн цэс
          </text>
                            <text style={{ marginTop: 10, color: 'white', fontSize: 13 }} >Нүүр  /  Бидний тухай  /  Захиргаа  /  Сургалт  /  Эрдэм шинжилгээ  /  Салбарууд  /  Оюутан</text>

                        </div>
                        <div className="secondfooterfirstdiv">
                            <text style={{ fontSize: 20, color: 'yellow' }}>
                                Бидэнтэй холбогдох
          </text>
                            <text style={{ marginTop: 10, color: 'white', fontSize: 13 }} >Монгол улс, Улаанбаатар хот
                            Баянзүрх дүүрэг,22-р хороо
                            Утас: 976-70151333
              Сургалтын алба: 70152333</text>
                        </div>
                    </div>
                </StickyFooter>
                {/* </div> */}
            </div>
        )
    }
}

export default Surgalt;

const data = ['Дээд боловсрол'];
function MenuData({ data }) {
    return <div style={{ marginTop: 15 }}>
        <div className="menuData"><text style={{ color: '#5D6D7E' }}>Бидний тухай</text></div>
        {data.map((item, index) => {
            return <div className="menuData"><text className="myFontText">{item}</text></div>
        })}
    </div>
}