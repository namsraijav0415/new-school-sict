import React, { Component } from 'react';
// import './App.css';
import { Layout, Header, Navigation, Drawer, Content } from 'react-mdl';
import { Link } from 'react-router-dom';
import Main from './main';

let val = 1;

class Loggedin extends Component {
  componentDidMount() {
    window.open('http://localhost:3000/showinfo');
  }
  render() {

    if(val == 1) {
      return (
        <div className="demo-big-content">
          <Layout>
            <Header className="header-color" title={<Link style={{ textDecoration: 'none', color: 'white' }} to="/"></Link>} scroll>
              <Navigation>
                <Link to="/Home">Нүүр</Link>
                <Link to="/abouchangeInfotme">Мэдээлэл засах</Link>
                <Link to="/zahirgaa">Захиргаа</Link>
                <Link to="/surgalt">Сургалт</Link>
                <Link to="/erdem">Эрдэм шижилгээ</Link>
                <Link to="/aboutme">Салбарууд</Link>
                <Link to="/projects">Оюутан</Link>
                <Link to="/contact">Сургалтын төв</Link>
              </Navigation>
            </Header>
            <Drawer title={<Link style={{ textDecoration: 'none', color: 'black' }} to="/"></Link>}>
              <Navigation>
                <Link to="/resume">Нүүр</Link>
                <Link to="/changeInfo">Мэдээлэл засах</Link>
                <Link to="/projects">Захиргаа</Link>
                <Link to="/contact">Сургалт</Link>
                <Link to="/resume">Эрдэм шижилгээ</Link>
                <Link to="/aboutme">Салбарууд</Link>
                <Link to="/projects">Оюутан</Link>
                <Link to="/contact">Сургалтын төв</Link>
              </Navigation>
            </Drawer>
            {/* <button variant="btn btn-success" onClick={() => history.push('/Products')}>Click button to view products</button> */}

            <Content>
              <div className="page-content" />
              <Main />
            </Content>
          </Layout>
          </div>

      );
    }
    
    return (
      <div className="demo-big-content">
        <Layout>
          <Header className="header-color" title={<Link style={{ textDecoration: 'none', color: 'white' }} to="/"></Link>} scroll>
            <Navigation>
              <Link to="/Home">Нүүр</Link>
              <Link to="/aboutme">Бидний тухай</Link>
              <Link to="/zahirgaa">Захиргаа</Link>
              <Link to="/surgalt">Сургалт</Link>
              <Link to="/erdem">Эрдэм шижилгээ</Link>
              <Link to="/aboutme">Салбарууд</Link>
              <Link to="/projects">Оюутан</Link>
              <Link to="/contact">Сургалтын төв</Link>
            </Navigation>
          </Header>
          {/* <Drawer title={<Link style={{ textDecoration: 'none', color: 'black' }} to="/"></Link>}>
            <Navigation>
              <Link to="/resume">Нүүр</Link>
              <Link to="/aboutme">Бидний тухай</Link>
              <Link to="/projects">Захиргаа</Link>
              <Link to="/contact">Сургалт</Link>
              <Link to="/resume">Эрдэм шижилгээ</Link>
              <Link to="/aboutme">Салбарууд</Link>
              <Link to="/projects">Оюутан</Link>
              <Link to="/contact">Сургалтын төв</Link>
            </Navigation>
          </Drawer> */}
          <Content>
            <div className="page-content" />
            <Main />
          </Content>
        </Layout>
      </div>

    );
  }
}

export default Loggedin;
